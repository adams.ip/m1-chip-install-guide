# Apple Silicon M1 安裝 TensorFlow
如欲TensorFlow使用M1中的gpu，我們需要特別版本的Python，連同所需dependencies，一併安裝，才能成事。
**請留意 macOS 必須至少為 Montery v12.1**
## pyenv
<p>前情回顧，我們在macOS使用pyenv 去<strong>管理、區隔不同Python 版本</strong>，避免大意觸碰system原有的Python ，致使macOS崩壞，同時方便大家在不同Python 版本之間轉換，譬如你按自己需要分別安裝了Python3.8.3 和 Python 3.9.9。</p>
<br>
<p><strong>注意!!</strong>不要與 virtual environment混淆，virtual environment 旨在<strong>區隔不同Projects 所安裝的packages</strong>。</p>
<br>

<p>接下來，我們也會繼續使用pyenv安裝另一Python版本 miniforge。此版本支援arm64 architecture，亦即是Apple M1。</p>

<p>記得brew install pyenv 後 執行<p>

for old macOS using bash
```

echo 'export PYENV_ROOT="$HOME/.pyenv"' >> ~/.profile
echo 'export PATH="$PYENV_ROOT/bin:$PATH"' >> ~/.profile
echo 'eval "$(pyenv init --path)"' >> ~/.profile
echo 'if [ -n "$PS1" -a -n "$BASH_VERSION" ]; then source ~/.bashrc; fi' >> ~/.profile

echo 'eval "$(pyenv init -)"' >> ~/.bashrc
```
for new macOS using Zsh
```

echo 'eval "$(pyenv init --path)"' >> ~/.zprofile
echo 'eval "$(pyenv init -)"' >> ~/.zshrc

##source : https://github.com/pyenv/pyenv#
```

**上面command run 完唔出野係正常的 **

<p>記得quit terminal，再開過。</p>
<p>試試於terminal輸入pyenv，enter 睇下有冇出pyenv usage，確保pyenv 在執行路徑中<p>

## 安裝
安裝準備，請確保你的mac 是使用m1 ，macOS版本為 Monterey 12.1或以上

```
pyenv install miniforge3
```

然後，我們將建立一個folder 
```
mkdir demo-tensorflow-metal

cd demo-tensorflow-metal
```

並設定當我們進入這個folder 時，啟用Python miniforge3 版本
```
pyenv local miniforge3
```

可以檢查當前

繼續安裝所需的dependencies，由於當你使用miniforge3 必然會是使用M1 gpu，因此以下安裝都會在全局（global），不會setup virtual environment。
```
conda install -c apple tensorflow-deps

python -m pip install tensorflow-macos
python -m pip install tensorflow-metal
```

## 使用
如想在Visual Studio Code 使用 jupyter notebook，需要先安裝一個 execution backend : <strong>ipykernel</strong>
```
python -m pip install ipykernel
```

並在Visual Studio Code <strong>Run</strong>之前，選定 miniforge 3 為當前execution kernel。
![](./img/vscode.png)


可以使用 sample code [Keras MNIST Demo](https://keras.io/examples/vision/mnist_convnet/) 去測試是否運作正常。

簡單快捷方式去檢視 tensorflow 是否在使用m1 gpu :
1. 開啟 Activity monitor
2. 查看GPU usage
![](./img/activity_monitor.png)
